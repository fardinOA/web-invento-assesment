import { Inter } from "next/font/google";
import Posts from "../components/posts";
import { useDispatch, useSelector } from "react-redux";
import {
    addPost,
    clearSuccess,
    fetchPosts,
    updatePost,
} from "../redux/features/post/postSlice";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");
    const [addModal, setAddModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);

    const { post, addPostSuccess } = useSelector((state) => state.posts.post);
    const { editablePost, editSucess, deletePostSuccess } = useSelector(
        (state) => state.posts
    );

    const [editableTitle, setEditableTitle] = useState("");
    const [editableBody, setEditableBody] = useState("");

    const dispatch = useDispatch();
    useEffect(() => {
        setEditableBody(editablePost.body);
        setEditableTitle(editablePost.title);
        if (addPostSuccess) {
            toast.success("A new post added", {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
            dispatch(clearSuccess());
            setBody("");
            setTitle("");
        }
        if (editSucess) {
            toast.success(
                JSON.stringify({
                    title: editablePost.title,
                    body: editablePost.body,
                }),
                {
                    position: "top-center",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                }
            );
            dispatch(clearSuccess());
        }
        if (deletePostSuccess) {
            toast.success("Delete Success", {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
            dispatch(clearSuccess());
        }
    }, [
        dispatch,
        editablePost.body,
        editablePost.title,
        post,
        editSucess,
        addPostSuccess,
        deletePostSuccess,
    ]);
    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);
    useEffect(() => {
        if (updateModal) {
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
        }
    }, [updateModal]);

    const submitHandeler = (e) => {
        e.preventDefault();
        dispatch(addPost({ title, body }));
        setAddModal(false);
    };

    const submitEditHandeler = (e) => {
        e.preventDefault();

        dispatch(
            updatePost({
                title: editableTitle,
                body: editableBody,
                id: editablePost.id,
            })
        );
        setUpdateModal(false);
    };

    return (
        <div className="  w-full relative ">
            <div className={`${(addModal || updateModal) && "blur-sm"}`}>
                <div className=" flex justify-center text-black p-4 ">
                    <button
                        onClick={() => setAddModal(!addModal)}
                        className=" cursor-pointer text-center p-2 rounded-md mx-auto   hover:shadow-xl transition-all w-full bg-gradient-to-r from-[#B9F3E4] to-[#FFD6EC] "
                    >
                        Add
                    </button>
                </div>
                <div className=" flex justify-center items-center w-full h-full ">
                    <Posts
                        updateModal={updateModal}
                        setUpdateModal={setUpdateModal}
                    />
                </div>
            </div>
            {/* modal */}

            {addModal && (
                <div className=" rounded-md w-[90%] sm:w-[30rem]   bg-[#FEFCF3] border border-black shadow-xl p-4 absolute top-[20rem] left-1/2 transform -translate-x-1/2 -translate-y-1/2">
                    {" "}
                    <div className="  relative" htmlFor="">
                        <h3 className="text-lg font-bold border-b border-black w-fit mx-auto ">
                            Add Post
                        </h3>
                        <div className=" my-8 space-y-4 " action="">
                            <input
                                className=" text-black border rounded-md py-1 px-2 focus:outline-none w-full  "
                                type="text"
                                placeholder="Title"
                                name="title"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />

                            <textarea
                                name="body"
                                value={body}
                                onChange={(e) => setBody(e.target.value)}
                                placeholder="Body"
                                className="  border rounded-md py-1 px-2 focus:outline-none text-[18px] w-full    outline-none  "
                            ></textarea>
                            <button
                                onClick={(e) => submitHandeler(e)}
                                type="submit"
                                className="text-black bg-blue-300 py-1 px-2 w-full text-center shadow-xl hover:scale-[1.02] transition-transdiv "
                            >
                                Add Post
                            </button>
                        </div>
                        <button
                            className=" absolute top-[-10px] right-[-10px] "
                            onClick={() => setAddModal(!addModal)}
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
            )}

            {updateModal && (
                <div className=" rounded-md w-[90%] sm:w-[30rem]   bg-[#FEFCF3] border border-black shadow-xl p-4 absolute top-[20rem] left-1/2 transform -translate-x-1/2 -translate-y-1/2">
                    {" "}
                    <div className="  relative" htmlFor="">
                        <h3 className="text-lg font-bold border-b border-black w-fit mx-auto ">
                            Edit Post
                        </h3>
                        <div className=" my-8 space-y-4 " action="">
                            <input
                                className=" text-black border rounded-md py-1 px-2 focus:outline-none w-full  "
                                type="text"
                                placeholder="Title"
                                name="title"
                                value={editableTitle}
                                onChange={(e) =>
                                    setEditableTitle(e.target.value)
                                }
                            />

                            <textarea
                                name="body"
                                value={editableBody}
                                onChange={(e) =>
                                    setEditableBody(e.target.value)
                                }
                                placeholder="Body"
                                className="    border rounded-md py-1 px-2 focus:outline-none text-[18px] w-full    outline-none  "
                            ></textarea>
                            <button
                                onClick={(e) => submitEditHandeler(e)}
                                type="submit"
                                className="text-black bg-blue-300 py-1 px-2 w-full text-center shadow-xl hover:scale-[1.02] transition-transdiv "
                            >
                                Update Post
                            </button>
                        </div>
                        <button
                            className=" absolute top-[-10px] right-[-10px] "
                            onClick={() => setUpdateModal(!updateModal)}
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
}
