import axios from "axios";

export const allPost = async () => {
    const { data } = await axios.get(
        `https://jsonplaceholder.typicode.com/posts`
    );

    return data;
};

export const addPostApi = async (postData) => {
    const config = {
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    };
    const { data } = await axios.post(
        `https://jsonplaceholder.typicode.com/posts`,
        {
            ...postData,
            userId: 1,
        },
        config
    );

    return data;
};

export const updatePostApi = async (postData) => {
    const config = {
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    };

    const { data } = await axios.put(
        `https://jsonplaceholder.typicode.com/posts/${postData.id}`,
        {
            ...postData,
            userId: 1,
        },
        config
    );

    return data;
};

export const deletePostApi = async (id) => {
    const config = {
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    };

    const { data } = await axios.delete(
        `https://jsonplaceholder.typicode.com/posts/${id}`,

        config
    );

    return data;
};
