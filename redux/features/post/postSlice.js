import { addPostApi, allPost, deletePostApi, updatePostApi } from "./postApi";

const { createAsyncThunk, createSlice } = require("@reduxjs/toolkit");

const initialState = {
    isLoading: false,
    error: null,
    isError: false,
    posts: [],
    post: {
        addPostSuccess: false,
        isLoading: false,
        error: null,
        isError: false,
    },
    editableLoading: false,
    editSucess: false,
    editError: null,
    editablePost: {},
    deletePostSuccess: false,
    deltePostLoading: false,
};

export const fetchPosts = createAsyncThunk("post/fetchPosts", async () => {
    const posts = await allPost();

    return posts;
});

export const addPost = createAsyncThunk("post/addPost", async (data) => {
    const post = await addPostApi(data);
    return post;
});

export const updatePost = createAsyncThunk("post/updatePost", async (data) => {
    const post = await updatePostApi(data);
    return post;
});
export const deletePost = createAsyncThunk("post/deletePost", async (data) => {
    const post = await deletePostApi(data);
    return post;
});

// create slice

const postSlice = createSlice({
    name: "post",
    initialState,
    reducers: {
        addEditablePost: (state, action) => {
            state.editablePost = action.payload;
            state.editableLoading = false;
        },
        clearSuccess: (state, action) => {
            state.editSucess = false;
            state.post.addPostSuccess = false;
            state.deletePostSuccess = false;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchPosts.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(fetchPosts.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.posts = action.payload;
            })
            .addCase(fetchPosts.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.posts = [];
            })
            .addCase(addPost.pending, (state, action) => {
                state.post.isLoading = true;
                state.post.error = null;
            })
            .addCase(addPost.fulfilled, (state, action) => {
                state.post.isLoading = false;
                state.post.post = action.payload;
                state.post.addPostSuccess = true;
            })
            .addCase(addPost.rejected, (state, action) => {
                state.post.isLoading = false;
                state.post.isError = true;
                state.post.addPostSuccess = false;
                state.post.error = action.error?.message;
                state.post.post = {};
            })
            .addCase(updatePost.pending, (state, action) => {
                state.editableLoading = true;
            })
            .addCase(updatePost.fulfilled, (state, action) => {
                state.editableLoading = false;
                state.editablePost = action.payload;
                state.editSucess = true;
            })
            .addCase(updatePost.rejected, (state, action) => {
                state.editableLoading = false;
                state.editError = action.error?.message;
                state.editablePost = {};
            })
            .addCase(deletePost.pending, (state, action) => {
                state.deltePostLoading = true;
                state.deletePostSuccess = false;
            })
            .addCase(deletePost.fulfilled, (state, action) => {
                state.deletePostSuccess = true;
            })
            .addCase(deletePost.rejected, (state, action) => {
                state.deletePostSuccess = false;

                state.deltePostLoading = false;
            });
    },
});

export default postSlice.reducer;
export const { addEditablePost, clearSuccess } = postSlice.actions;
