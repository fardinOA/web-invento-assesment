import React from "react";
import { useDispatch } from "react-redux";
import {
    addEditablePost,
    deletePost,
} from "../../redux/features/post/postSlice";

const PostCard = ({ post, updateModal, setUpdateModal }) => {
    const dispatch = useDispatch();

    return (
        <div className=" text-black capitalize h-fit p-2 rounded-md bg-[#B9F3E4] sm:w-[25rem] mx-auto ">
            <div className="shadow-xl flex justify-between bg-[#EA8FEA] py-1 px-2 rounded-t-md">
                <h1 className=" w-[20%] ">{post.id}</h1>
                <h1 className=" text-right ">{post.title}</h1>
            </div>
            <div className="  shadow-xl mt-4 bg-[#FFAACF] py-1 px-2  rounded-sm ">
                {post.body}
            </div>
            <div className="   mt-4 py-1 px-2   rounded-b-md flex justify-between ">
                <button
                    onClick={() => {
                        setUpdateModal(!updateModal);
                        dispatch(addEditablePost({ ...post }));
                    }}
                    className=" hover:scale-105 hover:shadow-xl transition-all w-full bg-gradient-to-r from-[#B9F3E4] to-[#FFD6EC]"
                >
                    Edit
                </button>
                <button
                    onClick={() => {
                        dispatch(deletePost(post.id));
                    }}
                    className="  hover:scale-105 hover:shadow-xl transition-all w-full bg-gradient-to-r from-[#FFD6EC] to-[#B9F3E4]"
                >
                    Delete
                </button>
            </div>

            {/* modal for edit post */}

            {/* modal for edit post */}
        </div>
    );
};

export default PostCard;
