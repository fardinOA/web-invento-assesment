import React from "react";
import { useSelector } from "react-redux";
import PostCard from "./PostCard";

const Posts = ({ updateModal, setUpdateModal }) => {
    const { posts } = useSelector((state) => state.posts);
    return (
        <div
            className={` ${
                updateModal && "  "
            } p-4 flex flex-col flex-wrap gap-6 md:gap-[2.5rem] md:flex-row`}
        >
            {posts.map((post, ind) => (
                <PostCard
                    updateModal={updateModal}
                    setUpdateModal={setUpdateModal}
                    key={`json-post-${ind}`}
                    post={post}
                />
            ))}
        </div>
    );
};

export default Posts;
